###############################
##           Base            ##
###############################
FROM alpine:3.19 AS base

# params
ARG NODE_ENV=production
ARG UID=1000
ARG GID=1000
ARG UNAME=dev

# environment variables
ENV CI=1
ENV NODE_ENV=$NODE_ENV

# Create same user inside container
WORKDIR /app
RUN addgroup -S -g $GID $UNAME && \
    adduser -S -u $UID -G $UNAME $UNAME && \
    addgroup $UNAME root && \
    chown -R $UNAME:$UNAME /app

# install Node
RUN apk add --no-cache bash nodejs npm && \
    rm -rf /var/cache/apk/*


###############################
##           Vite            ##
###############################
FROM base AS vite
USER $UNAME

# Vite environment
COPY --chown=$UNAME:$UNAME vite /app/vite
RUN cd /app/vite && \
    npm ci --include=dev && \
    npm cache clean --force

WORKDIR /app/vite
ENTRYPOINT ["/bin/bash"]


###############################
##    Android sdkmanager     ##
###############################
FROM vite AS android
WORKDIR /app
USER root

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk
ENV ANDROID_HOME=/opt/android_sdk
ENV PATH="${PATH}:${ANDROID_HOME}/build-tools"
ENV PATH="${PATH}:${ANDROID_HOME}/cmdline-tools/latest/bin"
ENV PATH="${PATH}:${ANDROID_HOME}/platform-tools"
ENV PATH="${PATH}:${ANDROID_HOME}/tools"
ENV PATH="${PATH}:${ANDROID_HOME}/tools/bin"
ENV PATH="${PATH}:/opt/gradle/gradle-7.6.4/bin"

# install packages
RUN apk add --no-cache \
      libc6-compat \
      openjdk11 && \
    rm -rf /var/cache/apk/*

# install android SDK & Gradle
RUN mkdir -p /opt/android_sdk/cmdline-tools && \
      wget -O /tmp/commandlinetools.zip https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip && \
      unzip -q /tmp/commandlinetools.zip -d /opt/android_sdk/cmdline-tools && \
      mv /opt/android_sdk/cmdline-tools/cmdline-tools /opt/android_sdk/cmdline-tools/latest && \
      rm -f /tmp/commandlinetools.zip && \
    mkdir -p /opt/gradle && \
      wget -O /tmp/gradle.zip https://services.gradle.org/distributions/gradle-7.6.4-bin.zip && \
      unzip -q /tmp/gradle.zip -d /opt/gradle && \
      rm -f /tmp/gradle.zip && \
    yes | sdkmanager "platform-tools" "platforms;android-33" "build-tools;33.0.2"


###############################
##         Cordova           ##
###############################
FROM android AS cordova

USER $UNAME

# Cordova environment
COPY --chown=$UNAME:$UNAME cordova /app/cordova
RUN cd /app/cordova && \
    cp ./scripts/config.xml ./config.xml && \
    npm i cordova@12.0.0 && \
    npm i --include=dev && \
    npm cache clean --force

# Cache deps to optimize build performance
COPY --chown=$UNAME:$UNAME app/config.json /app/vite/app/config.json
RUN cd /app/vite && \
      touch ./app/index.html && \
      npm run build && \
      mv /app/vite/build /app/cordova/www && \
    cd /app/cordova && \
      npx cordova prepare && \
      npx cordova build && \
      npx cordova clean && \
      rm -rf "/app/vite/app" "/app/cordova/www"


###############################
##          Build            ##
###############################
FROM cordova AS build

SHELL ["/bin/bash", "-c"]
ENTRYPOINT cd /app/vite && \
    npm run build && \
    mv /app/vite/build /app/cordova/www && \
    cd /app/cordova && \
    rm -r /app/cordova/platforms && \
    npm i && \
    npx cordova prepare && \
    npx cordova build
