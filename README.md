# 🚀 Get Started

## Before starting...

Before you can use this project, make sure you have Docker installed on your machine. For detailed instructions on installing Docker, please refer to the official [Docker website](https://www.docker.com/get-started/).


## 1. Download your starter App

Download [the app folder](https://gitlab.com/GuilleW/docker-cordova-vue/-/archive/main/docker-cordova-vue-main.zip?path=app) in this repo and `cd` inside, or copy/past this command in shell ...


```sh
wget -qO tmp.zip "https://gitlab.com/GuilleW/docker-cordova-vue/-/archive/main/docker-cordova-vue-main.zip?path=app" && unzip -oqq tmp.zip "docker-cordova-vue-main-app/app/*" -d . && mv ./docker-cordova-vue-main-app/app docker-cordova-vue-app && rm -R ./docker-cordova-vue-main-app tmp.zip && cd docker-cordova-vue-app
```
GG ! You have your "ready to use" source code of your new Awsome Android Web App!


## 2. Dev your App

##### Use Hot-Reload for Development
```sh
make vite
make cordova
```

##### Lint with [ESLint](https://eslint.org/)

```sh
make lint
```

##### and Format with [Prettier](https://prettier.io/)

```sh
make format
```

##### You can Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
make test
```

##### Debug on device
Plug your device into dev mode and use Chrome to debug your app. Run this command to start a watcher so that every time you build your app, it pushes it to your device.

```sh
make droid
```

## 3. Build It !
```bash
make build
```
Your builds are stored in `release`, in your app folder. Enjoy!


## (Extra)
If you need to test something in the container, juste run :
```bash
make it
```

Uninstall everything with :
```bash
make clean
```

## Contribution

Contributions are welcome! Feel free to submit [issues](https://gitlab.com/GuilleW/docker-cordova-vue/-/issues) or [feature requests](https://gitlab.com/GuilleW/docker-cordova-vue/-/issues).

## License

This project is licensed under the [GPL-3.0-or-later](https://gitlab.com/GuilleW/docker-cordova-vue/-/blob/main/LICENSE) license.
