import 'core-js/actual'
import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// Vuetify
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const init = () => {
  const vuetify = createVuetify({
    components,
    directives,
    theme: {
      defaultTheme: 'dark'
    }
  })

  const app = createApp(App)
  app.use(createPinia())
  app.use(router)
  app.use(vuetify)
  app.provide('$cordova', window.cordova)
  app.mount('#app')
}

document.addEventListener('deviceready', () => {
  console.log('ready !')
  init()
})

// If we are not in Cordova, manually trigger the deviceready event
if (!window.cordova) {
  document.dispatchEvent(new CustomEvent('deviceready', {}))
}
