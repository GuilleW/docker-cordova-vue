import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useTheme } from 'vuetify'

export const useSettingsStore = defineStore('settings', () => {
  const storage = window.localStorage

  // settings.theme
  const appTheme = useTheme()
  const theme = ref(storage.getItem('settings.theme') || appTheme.global.name.value)

  function setTheme(value) {
    appTheme.global.name.value = value
    storage.setItem('settings.theme', value)
    theme.value = value
  }

  // settings.anotherSettingHere
  // ...

  return { theme, setTheme }
})
