const fs = require('fs')

module.exports = function (context) {
  const file = '/app/cordova/www/hotreload-timestamp.txt'
  const content = Date.now().toString()
  fs.writeFileSync(file, content)
}
