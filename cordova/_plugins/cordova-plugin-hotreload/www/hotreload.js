setInterval(() => {
  const url = '/hotreload-timestamp.txt'
  fetch(url)
    .then((response) => {
      if (!response.ok) return
      response.text().then((timestamp) => {
        if (window.hotreload === undefined) {
          window.hotreload = timestamp
        } else if (window.hotreload !== timestamp) {
          window.location.reload()
        }
      })
    })
    .catch((error) => {
      console.error(`Error fetching ${url}:`, error)
    })
}, 1000)
