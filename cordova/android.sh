#!/bin/bash

buffer_terminal=""

apk_pathfile() {
  local directory="/app/vite/app/release"
  local extension="apk"
  local latest_file=""
  local latest_timestamp=0
  local file=""

  for file in "$directory"/*; do
    if [ -f "$file" ] && [[ "$file" == *".$extension" ]]; then
      local timestamp=$(stat -c %Y "$file")
      if [ $timestamp -gt $latest_timestamp ]; then
        latest_timestamp=$timestamp
        latest_file=$file
      fi
    fi
  done

  echo "$latest_file"
}

apk_name() {
  local id=$(grep -o 'id="[^"]*"' /app/cordova/config.xml | sed 's/id="\([^"]*\)"/\1/')
  echo "$id"
}

apk_timestamp() {
  local directory="/app/vite/app/release"
  local extension="apk"
  local latest_timestamp=0
  local file=""

  for file in "$directory"/*; do
    if [ -f "$file" ] && [[ "$file" == *".$extension" ]]; then
      local timestamp=$(stat -c %Y "$file")
      if [ $timestamp -gt $latest_timestamp ]; then
        latest_timestamp=$timestamp
      fi
    fi
  done

  echo "$latest_timestamp"
}

# clean_terminal "str"
clean_terminal() {
  if [ -z "$1" ]; then
    return
  fi

  local i=1
  local lines=$(echo -e "$1" | wc -l)

  for ((i=1; i<=$lines; i++)); do
    echo -e -n "\033[1A\033[K"
  done
}

# log_terminal "str"
log_terminal() {
  local str=${1:-""}
  clean_terminal "$buffer_terminal"
  buffer_terminal="$str"
  echo -e "$str"
}

# log_and_wait "str" {int|str}
log_and_wait() {
  local i=0
  local str=${1:-""}
  local wait=${2:-0}
  local wait_str="Press Enter to continue"

  if ! [[ "$wait" =~ ^[0-9]+$ ]]; then
    wait_str="$wait"
    wait=0
  fi

  if [ $wait -gt 0 ]; then
    for ((i=0; i<$wait; i++)); do
      if [[ $((wait - i)) -gt 1 ]]; then
        log_terminal "$str \n\nNew attempt in $(($wait-$i)) seconds ..."
      else
        log_terminal "$str \n\nNew attempt in 1 second ..."
      fi
      sleep 1
    done
  else
    log_terminal "$str \n\n$wait_str"
    read -n 1 -s
  fi
  log_terminal "$str"

}

# color "str" "color"
color() {
  local text="$1"
  local color="$2"

  case "$color" in
    "grey")
      echo -e "\033[90m$text\033[0m"
      ;;
    "green")
      echo -e "\e[32m$text\e[0m"
      ;;
    "blue")
      echo -e "\e[34m$text\e[0m"
      ;;
    "red")
      echo -e "\033[31m$text\033[0m"
      ;;
    *)
      echo "$text"
      ;;
  esac
}

# output "step1" "step2" "step3" "step4"
output() {
  # default values
  tick1="[ ]"
  tick2="[ ]"
  tick3="[ ]"
  tick4="[ ]"
  tick5="[ ]"
  step1=${1:-"ADB started"}
  step2=${2:-"USB debugging allowed"}
  step3=${3:-"APK $(apk_name) installed"}
  step4=${4:-"APK started on device"}
  step5="Waiting for a new build"

  if [ -n "$1" ]; then
    step1=$(color "$step1" "blue")
    step2=$(color "$step2" "grey")
    step3=$(color "$step3" "grey")
    step4=$(color "$step4" "grey")
    step5=$(color "$step5" "grey")
  fi
  if [ -n "$2" ]; then
    tick1="[$(color "✔" "green")]"
    step2=$(color "$step2" "blue")
    step3=$(color "$step3" "grey")
    step4=$(color "$step4" "grey")
    step5=$(color "$step5" "grey")
  fi
  if [ -n "$3" ]; then
    tick1="[$(color "✔" "green")]"
    tick2="[$(color "✔" "green")]"
    step3=$(color "$step3" "blue")
    step4=$(color "$step4" "grey")
    step5=$(color "$step5" "grey")
  fi
  if [ -n "$4" ]; then
    tick1="[$(color "✔" "green")]"
    tick2="[$(color "✔" "green")]"
    tick3="[$(color "✔" "green")]"
    step4=$(color "$step4" "blue")
    step5=$(color "$step5" "grey")
  fi
  if [ -n "$5" ]; then
    tick1="[$(color "✔" "green")]"
    tick2="[$(color "✔" "green")]"
    tick3="[$(color "✔" "green")]"
    tick4="[$(color "✔" "green")]"
    step5="Waiting for a new build\n\nOpen a new terminal and run : \"make build\""
    tick5="[$(color "-" "blue")]"
    step5=$(color "$step5" "blue")
  fi

  str=""
  str="$str$tick1  $step1\n"
  str="$str$tick2  $step2\n"
  str="$str$tick3  $step3\n"
  str="$str$tick4  $step4\n"
  str="$str$tick5  $step5\n"

  echo -e "$str"
}

parse_result() {
  local str=${1:-""}
  local wait=3

  if [[ "$str" == *"INSTALL_FAILED_UPDATE_INCOMPATIBLE"* ]]; then
    log_and_wait "$(output "" "" "" "Uninstall App on device, and continue ...")"
  elif [[ "$str" == *"Error: Activity class"* ]]; then
    log_and_wait "$(output "" "" "" "App name ( $(apk_name) ) not found on device.")" "CTRL+C to stop and check params ..."

  elif [[ "$str" == *"adb: failed to stat"* || "$str" == *"adb: filename doesn't end"* ]]; then
    log_and_wait "$(output "" "" "Apk not found ( $(apk_pathfile) ), install failed.")" "Press Enter when the APK is compiled ..."

  elif [[ "$str" == *"unauthorized"* ]]; then
    log_and_wait "$(output "" "Pending authentication: please accept debugging session on the device.")" $wait
  elif [[ "$str" == *"List of devices attached"* ]]; then
    log_and_wait "$(output "" "No devices detected. Please connect your device and enable USB debugging.")" $wait
  elif [[ "$str" == *"error: no devices/emulators found"* ]]; then
    log_and_wait "$(output "" "No devices detected. Please connect your device and enable USB debugging.")" $wait
  elif [[ "$str" == *"No such device"* || "$str" == *"adb: no devices/emulators found"* || "$str" == *"Connection reset by peer"* ]]; then
    log_and_wait "$(output "" "No such device (it may have been disconnected)")" $wait

  elif [[ "$str" == *"ADB server didn't ACK"* ]]; then
    log_and_wait "$(output "ADB server didn't ACK... restarting...")" $wait
  elif [[ "$str" == *"Access denied (insufficient permissions)"* ]]; then
    log_and_wait "$(output "ADB must be run as root.")" $wait
  else
    log_and_wait "$str" $wait
  fi

}

start_adb() {
  while true; do
    log_terminal "$(output "Starting ADB...")"
    local result=$(adb devices 2>&1)

    if [[ "$result" == *"List of devices attached"* ]]; then
      connect_to_device
    else
      parse_result "$result"
    fi

  done
}

connect_to_device() {
  while true; do
    log_terminal "$(output "" "Connecting to the device...")"
    local result=$(adb devices 2>&1)

    if [[ "$result" =~ [a-zA-Z0-9]+[[:space:]]+device ]]; then
      install_apk
    else
      parse_result "$result"
    fi

  done
}

install_apk() {
  while true; do
    log_terminal "$(output "" "" "Performing Streamed Install...")"
    adb uninstall $(apk_name) >/dev/null 2>&1
    local result="$(adb install -r $(apk_pathfile) 2>&1)"

    if [[ "$result" == *"Performing Streamed Install"* && "$result" == *"Success"* ]]; then
      start_apk
    else
      parse_result "$result"
      break
    fi

  done
}

start_apk() {
  log_terminal "$(output "" "" "Starting App...")"
  local result="$(adb shell am start -n $(apk_name)/$(apk_name).MainActivity 2>&1)"

  if [[ ("$result" == *"Starting: Intent"* && ! "$result" == *"Error: Activity class"*) || "$result" == *"to currently running top-most instance"* ]]; then
    watch_apk
  else
    parse_result "$result"
  fi
}

watch_apk() {
  local t="$(apk_timestamp)"
  log_terminal "$(output "" "" "" "" "ok")"
  while true; do
    if [[ ! "$t" == "$(apk_timestamp)" ]]; then
      break
    fi
    sleep 1
  done
}

start_adb
