const path = require('node:path')
const fs = require('node:fs')

module.exports = (ctx) => {
  const file = {
    config: path.join(ctx.opts.projectRoot, '../vite/app/config.json'),
  }

  // get app config data
  const config = JSON.parse(fs.readFileSync(file.config, 'utf8'))

  // create release path
  const releasePath = path.join(ctx.opts.projectRoot, '../vite/app/release')
  fs.mkdirSync(releasePath, { recursive: true })

  // build packages
  const packages = [
    {
      from: path.join(ctx.opts.projectRoot, 'platforms/electron/build', `${config.app.name}-${config.app.version}.AppImage`),
      to: path.join(releasePath, `${config.app.name}-${config.app.version}.AppImage`)
    },
    {
      from: path.join(ctx.opts.projectRoot, 'platforms/android/app/build/outputs/apk/debug/app-debug.apk'),
      to: path.join(releasePath, `${config.app.name}-${config.app.version}-debug.apk`)
    },
    {
      from: path.join(ctx.opts.projectRoot, 'platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk'),
      to: path.join(releasePath, `${config.app.name}-${config.app.version}-release-unsigned.apk`)
    }
  ]

  console.log('\n\nBuilt the following package(s):')
  packages.map(package => {
    try {
      if(fs.existsSync(package.from)) {
        fs.copyFileSync(package.from, package.to)
        console.log(`  - ${package.to}`)
      }
    }
    catch (err) {
      console.log(err)
    }
  })

}
