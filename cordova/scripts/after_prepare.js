const path = require('node:path')
const fs = require('node:fs')

module.exports = (ctx) => {
  const file = {
    index: path.join(ctx.opts.projectRoot, 'www/index.html')
  }

  // get index.html content and add cordova.js script
  if (fs.existsSync(file.index)) {
    let html = fs.readFileSync(file.index, 'utf8')
    if (!/cordova\.js/.test(html)) {
      html = html.replace('</body>', '  <script src="./cordova.js"></script>\n  </body>')
      fs.writeFileSync(file.index, html)
    }
  }
}
