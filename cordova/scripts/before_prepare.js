const path = require('node:path')
const fs = require('node:fs')
const { exec } = require('child_process')

module.exports = (ctx) => {
  const file = {
    config: path.join(ctx.opts.projectRoot, '../vite/app/config.json'),
    configSrc: path.join(ctx.opts.projectRoot, 'scripts/config.xml'),
    configDest: path.join(ctx.opts.projectRoot, 'config.xml'),
    buildJson: path.join(ctx.opts.projectRoot, 'build.json'),
    cordovaPlugins: path.join(ctx.opts.projectRoot, 'www/cordova_plugins.js')
  }

  // get app config data
  const config = JSON.parse(fs.readFileSync(file.config, 'utf8'))
  let xml = fs.readFileSync(file.configSrc, 'utf8')

  // update cordova config.xml with config app data
  for (const k in config.app) {
    const v = config.app[k]
    xml = xml.replace(`{{app.${k}}}`, v)
  }
  for (const k in config.author) {
    const v = config.author[k]
    xml = xml.replace(`{{author.${k}}}`, v)
  }
  fs.writeFileSync(file.configDest, xml)

  // write cordova build.json
  fs.writeFileSync(file.buildJson, JSON.stringify(config.build))

  // fix 404 /cordova_plugins.js if no plugins used
  fs.writeFileSync(file.cordovaPlugins, '')

  // install cordova plugins
  config["cordova-plugins"].forEach(e => {
    exec(`cordova plugin add ${e} --save`, (error, stdout, stderr) => {
      console.log(stdout)
      console.log(stderr)
      if (error !== null) console.log(`exec error: ${error}`)
    })
  })


}
