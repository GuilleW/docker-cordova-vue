import { exec } from 'child_process'

export default function CordovaPluginHotreload() {
  if (process.env.CORDOVA_HOTRELOAD !== 'true') return {}
  console.log('Hot Reload enabled for cordova')

  const execute = () => {
    const proc = exec(
      '\
      cd /app/vite && \
        npm run build && \
        rm -fr /app/cordova/www && \
        mv /app/vite/build /app/cordova/www && \
      cd /app/cordova && \
        npx cordova prepare && \
        npx cordova plugin add /app/cordova/_plugins/cordova-plugin-hotreload && \
        npx cordova run browser \
    ')
    proc.stdout.on('data', (data) => {
      if (!/^[0-9]+\s+\/hotreload-timestamp\.txt/.test(data) && !/^\n+$/.test(data)) {
        console.log(data)
      }
    })
    proc.stderr.on('data', (data) => {
      console.error(data)
    })
  }

  return {
    name: 'cordova-plugin-hotreload',

    buildStart() {
      if (process.argv.length !== 2) return
      execute()
    },
    handleHotUpdate({ file }) {
      console.log(`${file} changed... Reload cordova...`)
      execute()
    },
  }
}
