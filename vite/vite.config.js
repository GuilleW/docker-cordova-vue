import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VueDevTools from 'vite-plugin-vue-devtools'
import VitePluginFetch from 'vite-plugin-fetch'
import CordovaPluginHotreload from './plugins/hotreload.js'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), VueDevTools(), VitePluginFetch(), CordovaPluginHotreload()],
  root: 'app',
  base: './',
  build: {
    outDir: '../build'
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('/src', import.meta.url))
    }
  }
})
